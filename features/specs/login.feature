#language: pt
    @login
    Funcionalidade: Fazer a autenticação na plataforma Cotabest
    Autenticação como Fornecedor 
    Autenticação como Cliente PF E CNPJ
        

    Contexto: Acessar o site Cotabest 
        Dado que eu usuário deseje acessar o site da Cotabest

@login_fornecedor
Cenário: Logar com  Perfil Fornecedor 
Quando preencho os meus dados de login e senha
Então o sistema deve redirecionar para página inicial da Cotabest exibindo o Catalogo

@login_cpf
Cenário: Logar com  Perfil Cliente PF
Quando preencho os meus dados de login e senha
Então o sistema deve redirecionar para página inicial da Cotabest exibindo o Catalogo 

@login_cnpj
Cenário: Logar com  Perfil Cliente CNPJ
Quando preencho os meus dados de login e senha
Então o sistema deve redirecionar para página inicial da Cotabest exibindo o Catalogo

@reset_senha
Cenário: Reset de senha 
Quando submeto Esqueci minha senha 
Então o sistema deve redirecionar e solicitar para inserir um e-mail para a recuperação de senha
E preencho o email e submeto recuperar senha 
E devo receber um email para cadastrar nova senha  

