#language: pt
    @Compra_Cliente
    Funcionalidade: Realizar Uma Compra
        Realizar uma compra na plataforma Cotabest 

    Contexto: Acessar o site Cotabest
        Dado que eu esteja na página Cotabest

    @Login_clienteCNPJ_Forma_de_Pagamento_PIX
    Cenário: Compra com perfil cliente CNPJ - forma de Pagamento PIX
       
    Quando acesso com usuário cliente CNPJ 
            E faço a busca do produto para adicionar no carrinho 
            E acesso meu carrinho com produtos selecionados 
            E fecho os pedidos que estão no carrinho
            E realizo o pagamento com Pix
        Então eu finalizo meu pedido com sucesso

    @login_clienteCNPJ_Forma_de_Pagamento_VR
    Cenário: Compra com perfil cliente CNPJ - forma de Pagamento VR

    Quando adiciono um produto do fornecedor PMG no meu carrinho 
            E faço a minha compra com forma de pagamento VR 
        Então eu finalizo meu pedido com sucesso 

    @login_clienteCNPJ_Forma_de_Pagamento_Cartão_de_Credito
    Cenário: Compra com perfil cliente CNPJ - forma de Pagamento Cartao de Credito

    Quando faço compras de produto com fornecedor PMG e Empório Gênova 
            E faço a minha compra com forma de pagamento Cartão de credito 
        Então eu finalizo meu pedido com sucesso 

    @login_clienteCPF_Forma_de_Pagamento_PIX
    Cenário: Compra com perfil cliente CNPJ - forma de Pagamento PIX
       
    Quando acesso com usuário cliente CNPJ 
            E faço a busca do produto para adicionar no carrinho 
            E acesso meu carrinho com produtos selecionados 
            E fecho os pedidos que estão no carrinho
            E realizo o pagamento com Pix
        Então eu finalizo meu pedido com sucesso

    @login_clienteCPF_Forma_de_Pagamento_VR
    Cenário: Compra com perfil cliente CNPJ - forma de Pagamento VR

    Quando adiciono um produto do fornecedor PMG no meu carrinho 
            E faço a minha compra com forma de pagamento VR 
            Então eu finalizo meu pedido com sucesso 

    @login_clienteCPF_Forma_de_Pagamento_Cartão_de_Credito
    Cenário: Compra com perfil cliente CNPJ - forma de Pagamento Cartao de Credito

    Quando faço compras de produto com fornecedor PMG e Empório Gênova 
            E faço a minha compra com forma de pagamento Cartão de credito 
            Então eu finalizo meu pedido com sucesso

  