#language:pt
@cadastro_de_cliente
Funcionalidade: Fazer cadastro no site CotaBest
  Sendo um cliente da CotaBest
  Quero fazer o meu cadastro
  Para que eu possa fazer minhas compras

  @cpf
  Cenario: Preencher todos os dados de cadastro para comprar com CPF

    Dado que acesso a página de cadastro
      E clico em prosseguir
    Quando submeto o meu cadastro com:
      | NOME      | Analista  |
      | SOBRENOME | Qualidade |
      | EMAIL     | raquelmg888@gmail.com          |
      | SENHA     | 123456    |
      E clico na checkbox para aceitar os termos de uso
      E clico em AVANÇAR
      E sou redirecionado para a pagina de escolha de compra com CPF ou CNPJ
      E clico em comprar com CPF
      E sou redirecionado para a pagina de completar o meu cadastro
    Quando completo os meus dados com:
      | CPF                  | 499.739.710-51       |
      | DATA DE NASCIMENTO   | 04/02/1988           |
      | CELULAR              | 11932658523          |
      | TELEFONE ALTERNATIVO | 11932658542          |
      | SEGMENTO             | Compro para Consumir |
      E completo meu endereço com:
      | ESTADO      | AP               |
      | CIDADE      | Oiapoque         |
      | BAIRRO      | Centro           |
      | ENDEREÇO    | Av. Veiga Cabral |
      | NÚMERO      | 1986             |
      | COMPLEMENTO | Loja 1           |
      | CEP         | 68980000         |
    Então devo ser redirecionado para a pagina de catálogo

  @cnpj
  Cenario: Preencher todos os dados de cadastro para comprar com CNPJ

    Dado que acesso a página de cadastro
      E clico em prosseguir
    Quando submeto o meu cadastro com:
      | NOME      | Analista  |
      | SOBRENOME | Qualidade |
      | EMAIL     | raquelcnpj@gmail.com  |
      | SENHA     | 123456    |
      E clico na checkbox para aceitar os termos de uso
      E clico em AVANÇAR
      E sou redirecionado para a pagina de escolha de compra com CPF ou CNPJ
      E clico em comprar com CNPJ
      E sou redirecionado para a pagina de completar o meu cadastro
    Quando submeto os meus dados com:
      | CNPJ               | 34.952.791/0001-71        |
      | INSCRIÇÃO ESTADUAL |  815.734.847.682          |
      | RAZÃO SOCIAL       |   Webb Tec                |
      | NOME FANTASIA      |   Tec                     |
      | TELEFONE FIXO      |   1129565858             |
      | CELULAR            |   1196585858             |
      | SEGMENTO           | Academia ou Centro de Esportes |
      E completo meu endereço com:
      | ESTADO      | AP               |
      | CIDADE      | Oiapoque         |
      | BAIRRO      | Centro           |
      | ENDEREÇO    | Av. Veiga Cabral |
      | NÚMERO      | 1986             |
      | COMPLEMENTO | Loja 1           |
      | CEP         | 68980000         |
    Então devo ser redirecionado para a pagina de catálogo

  @email
  Cenario: Email já cadastrado

    Dado que acesso a página de cadastro
      E clico em prosseguir
    Quando submeto o meu cadastro com email ja cadastrado
      E clico na checkbox para aceitar os termos de uso
      E clico em AVANÇAR
    Então devo ver a mensagem E-mail em uso

  @data
  Cenario: Data de nascimento inválida

    Dado que acesso a página de cadastro
      E clico em prosseguir
    Quando submeto o meu cadastro com:
      | NOME      | Analista  |
      | SOBRENOME | Qualidade |
      | EMAIL     |           |
      | SENHA     | 123456    |
      E clico na checkbox para aceitar os termos de uso
      E clico em AVANÇAR
      E sou redirecionado para a pagina de escolha de compra com CPF ou CNPJ
      E clico em comprar com CPF
      E sou redirecionado para a pagina de completar o meu cadastro
    Quando completo os meus dados com uma data de nascimento inválida:
      | CPF                  |                      |
      | DATA DE NASCIMENTO   | 00/00/0000           |
      | CELULAR              | 11932658523          |
      | TELEFONE ALTERNATIVO | 11932658542          |
      | SEGMENTO             | Compro para Consumir |
      E completo meu endereço com:
      | ESTADO      | AP               |
      | CIDADE      | Oiapoque         |
      | BAIRRO      | Centro           |
      | ENDEREÇO    | Av. Veiga Cabral |
      | NÚMERO      | 1986             |
      | COMPLEMENTO | Loja 1           |
      | CEP         | 68980000         |
    Então devo ver a mensagem Atenção! Data de nascimento é obrigatório. E permanecer na pagina de cadastro.

  @sem_nome
  Cenario: Submeto o cadastro sem nome

    Dado que acesso a página de cadastro
      E clico em prosseguir
    Quando submeto o meu cadastro sem nome
      E clico na checkbox para aceitar os termos de uso
      E clico em AVANÇAR
    Então devo ver a mensagem Preencha este campo. Meu cadastro nao deve ser efetuado e devo permanecer na pagina de cadastro.

  @sem_sobrenome
  Cenario: Submeto o cadastro sem sobrenome

    Dado que acesso a página de cadastro
      E clico em prosseguir
    Quando submeto o meu cadastro sem sobrenome
      E clico na checkbox para aceitar os termos de uso
      E clico em AVANÇAR
    Então devo ver a mensagem Preencha este campo. Meu cadastro nao deve ser efetuado e devo permanecer na pagina de cadastro.

  @sem_email
  Cenario: Submeto o cadastro sem email

    Dado que acesso a página de cadastro
      E clico em prosseguir
    Quando submeto o meu cadastro sem email
      E clico na checkbox para aceitar os termos de uso
      E clico em AVANÇAR
    Então devo ver a mensagem Preencha este campo. Meu cadastro nao deve ser efetuado e devo permanecer na pagina de cadastro.

  @sem_senha
  Cenario: Submeto o cadastro sem senha

    Dado que acesso a página de cadastro
      E clico em prosseguir
    Quando submeto o meu cadastro sem senha
      E clico na checkbox para aceitar os termos de uso
      E clico em AVANÇAR
    Então devo ver a mensagem Preencha este campo. Meu cadastro nao deve ser efetuado e devo permanecer na pagina de cadastro.

  @sem_checkbox
  Cenario: Submeto o cadastro sem checkbox

    Dado que acesso a página de cadastro
      E clico em prosseguir
    Quando submeto o meu cadastro com os dados solicitados, porem não clico na checkbox para aceitar os termos de uso
      E clico em AVANÇAR
    Então devo ver a mensagem Marque esta caixa se deseja continuar. Meu cadastro nao deve ser efetuado e devo permanecer na pagina de cadastro.

