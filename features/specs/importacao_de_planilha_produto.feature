#language: pt
    @importação_planilha_produto
    Funcionalidade: Realizar Importação de Planilha 
        Realizar upload na planilha de importação de Produto 

    Contexto: Acessar o site Cotabest
        Dado que eu Fornecedor deseje fazer a importação de planilha de produto 

    @importação_planilha_com_EAN
        Cenário:Importar planilha de produto com EAN
        Quando eu importar  a planilha com EAN
        Então o sistema deve exibir mensagem de importação realizada com sucesso  


    @importação_planilha_sem_EAN
        Cenário: Importação de planilha de  importação sem EAN
        Quando eu importar  a planilha sem EAN
        Então o sistema deve exibir mensagem de importação realizada com sucesso, com as colunas no arquivo xls: Código, GTIN, Produto, Marca, Estoque, Unidade e Preço).

    @trava_upload_Preco
        Cenário: Trava de upload referente a preço
        Quando tem um item ou mais no arquivo que estão com uma variação de valor de 30% comparado com o ultimo arquivo importado
        Então o sistema deve enviar no e-mail para operação os itens no arquivo que estão divergentes com variação de preço acima do percentual permitido, o processo de upload não pode ser finalizado, e deve apresentar a mensagem: "Identificamos que durante o processo de upload alguns dos itens que se encontram no arquivo estão com uma variação de valor de 30% comparado com o ultimo arquivo, exibindo os itens no arquivo que estão divergentes com variação de preço acima do percentual".

