Dado('que eu esteja na página Cotabest') do
    visit 'https://staging.cotabest.com.br/'
end

Quando('acesso com usuário cliente CNPJ') do
    find("span", text: "Entre ou Cadastre-se").click
    find("input[name='username']").set "testecotabestCNPJ@hotmail.com"
    find("input[name='password']").set "123456"
    find("input[value='Entrar']").click
end

Quando('faço a busca do produto para adicionar no carrinho') do
    find("input[type='text']").set "geleia"
    find("button[data-pk='5002']").click
end

Quando('acesso meu carrinho com produtos selecionados') do
    find("img[class='header__cart-icon']").hover
end

Quando('fecho os pedidos que estão no carrinho') do
    expect(page).to_not have_content('Fechar pedidos')
end 

Quando('realizo o pagamento com Pix') do
    expect(page).to_not have_content('Transferência via Pix')
    sleep 2
end 

Quando('adiciono um produto do fornecedor PMG no meu carrinho') do
    find("input[name='q']").set "arroz 5kg camil"
    expect(page).to_not have_content('Adicionar ao carrinho')
    find("img[class='header__cart-icon']").hover
    expect(page).to_not have_content('Fechar pedidos')
    sleep 4
end

Quando('faço a minha compra com forma de pagamento VR') do
    expect(page).to_not have_content('Cartão de benefícios')
    find("input[type='text']").set "6370360003936892"
    find("input[name='form-0-benefits_card_name']").set "HOMOLOGACAO 000054"
    find("input[name='form-0-benefits_card_cpf']").set "640.584.031-01"
    find("option[value='9']").click_on
    find("option[value='21']").click_on
    find("input[name='form-0-benefits_card_cvv_number']").set "544"
    sleep 3
end

Quando('faço compras de produto com fornecedor PMG e Empório Gênova') do
    find("input[type='text']").set "banana"
    find("button[data-pk=43490']").click
end 

Quando('faço a minha compra com forma de pagamento Cartão de credito') do
    find("span[class='icon-cartao']").click
    find("input[placeholder='Digite o número do cartão']").set '5534 5508 1506 0098'
    find("input[placeholder='Digite o nome impresso no cartão']").set '06/10/2022'
    find("input[placeholder='Digite o número do CPF do titular do cartão']").set '847.903.290-11'
    find("select[name=form-0-credit_card_expiration_month']").set 'Outubro'
    find("select[name=form-0-credit_card_expiration_year']").set "2021"
    find("input[name=form-0-credit_card_cvv_number']").set "328"

end

Então('eu finalizo meu pedido com sucesso') do
    expect(page).to_not have_content('Finalizar pedido')
end






























































# Dado("que eu esteja na página de compras") do
#     visit 'http://automationpractice.com'
# end


# Quando("selecionar um produto na loja virtual") do
#     find("input[id='search_query_top']").set "blouses"
#     find("button[name='submit_search']").click
# end

# Quando("adicionar esse produto ao carrinho") do
#     find("img[title='Blouse']").hover
#     find("a[title='View']").click
#     find("p[id='add_to_cart']").click
#     find("a[class='btn btn-default button button-medium']").click
#     assert_text('Blouse')
#     find("a[class='button btn btn-default standard-checkout button-medium']").click
# end

# Quando("efetuar meu cadastro no site de compras") do
#     find("input[id='email_create']").set "#{Faker::Name.first_name}@mailinator.com"
#     find("button[id='SubmitCreate']").click

#     find("label[for='id_gender1']").click();
#     find("input[id='customer_firstname']").set Faker::Name.first_name 
#     find("input[id='customer_lastname']").set Faker::Name.last_name
#     find("input[id='passwd']").set "123456"
#     find("div[id='uniform-days']").select ('21')
#     find("div[id='uniform-months']").select ('April')
#     find("div[id='uniform-years']").select ('1980')
#     find("div[id='uniform-newsletter']").click
#     find("input[id='address1']").set Faker::Address.street_name
#     find("input[id='city']").set Faker::Address.city
#     find("div[id='uniform-id_state']").select ('Florida') 
#     find("input[id='postcode']").set "43199"
#     find("div[id='uniform-id_country']").select ('United States')
#     find("textarea[id='other']").set "Testes Campo Area"
#     find("input[name='phone_mobile']").set Faker::PhoneNumber.cell_phone
#     find("button[class='btn btn-default button button-medium']").click

#     find("button[name='processAddress']").click
#     sleep 2  

#     execute_script('window.scroll(0,250);')
#     within("p[class='checkbox']") do
#         find("div[id='uniform-cgv']" ).click();
#     end
#     find("button[class='button btn btn-default standard-checkout button-medium']").click 
# end

# Quando("realizar o pagamento") do
#     find("a[title='Pay by check.']").click
#     find("button[class='button btn btn-default button-medium']").click
#     sleep 2 
# end

# Então("eu finalizo o meu pedido com sucesso") do
#     assert_text('Your order on My Store is complete.')
#     execute_script('window.scroll(0,350);')
#     save_screenshot("data/#{Time.now.strftime('%HH%MM%SS')}pedido.png")  

#     find("a[title='Back to orders']").click
#     assert_text('ORDER HISTORY')
#     save_screenshot("data/#{Time.now.strftime('%HH%MM%SS')}pedido.png")
# end