Dado("que acesso a página de cadastro") do
  visit "https://staging.cotabest.com.br/login/" 
end

E("clico em prosseguir") do
  click_button "Prosseguir"
end

Quando("submeto o meu cadastro com:") do |table|
  find("#id_first_name").set "Analista"
  find("#id_surname").set "Qualidade"
  find("#id_email").set Faker::Internet.free_email
  find("#id_password").set "123456"
  sleep 2
end

E("clico na checkbox para aceitar os termos de uso") do
  check "id_terms"
end

E("clico em AVANÇAR") do
  click_button "AVANÇAR"
end

E("sou redirecionado para a pagina de escolha de compra com CPF ou CNPJ") do
  expect(page).to have_content "Crie sua conta na CotaBest" 
end

E("clico em comprar com CPF") do
  find("input[href='/profile-cpf/?cpf']").click
end  

E("sou redirecionado para a pagina de completar o meu cadastro") do
  expect(page).to have_content "Dados" 
end

Quando("completo os meus dados com:") do |table|                                                  
  find("#id_cpf").set Faker::CPF.numeric
  find("#id_birthdate").set Faker::Number.number(digits: 8)
  find("#id_phone").set "11932658523"
  find("#id_phone_alternative").set "11932658542"
  find("#id_job_segment-container").find("option", text: "Compro para Consumir").select_option    
  sleep 2         
end 

Quando("completo meu endereço com:") do |table|
  find("#id_address_state").find("option", text: "AP").select_option
  find("#id_address_city").find("option", text: "Oiapoque").select_option
  find("#id_address_district").set "Centro"
  find("#id_address_street").set "Av. Veiga Cabral"
  find("#id_address_number").set "1986"
  find("#id_address_compl").set "Loja 1"
  find("#id_address_cep").set "68980000"
  click_button "Salvar"
  sleep 2
end   

Então('devo ser redirecionado para a pagina de catálogo') do
  page.has_content?('Compre direto de lojas exclusivas!')
end

Quando('submeto o meu cadastro com email ja cadastrado') do
  find("#id_first_name").set "Raquel"
  find("#id_surname").set "Mendes"
  find("#id_email").set "testeqa.cotabest@gmail.com"
  find("#id_password").set "123456"
  sleep 2
end

Então('devo ver a mensagem E-mail em uso') do
  mensagem = find(".errorlist li")
  expect(mensagem.text).to eql "E-mail em uso"
end

Quando('completo os meus dados com uma data de nascimento inválida:') do |table|
  find("#id_cpf").set Faker::CPF.numeric
  find("#id_birthdate").set "00/00/000"
  find("#id_phone").set "11932658523"
  find("#id_phone_alternative").set "11532658542"
  find("#id_job_segment").find("option", text: "Compro para Consumir").select_option    
  sleep 2   
end

Então('devo ver a mensagem Atenção! Data de nascimento é obrigatório. E permanecer na pagina de cadastro.') do
  expect(page).to have_css ".consumer-address" 
end

Quando('clico em comprar com CNPJ') do
  click_on "Quero comprar com CNPJ"
end

Quando('submeto os meus dados com:') do |table|
  find('#id_cnpj').set Faker::CNPJ.numeric
  find('#id_social_name').set "Raquel QA"
  find('#id_fantasy_name').set "Analista QA"
  find('#id_phone').set "11532658523"
  find('#id_job_segment').find("option", text: "Academia ou Centro de Esportes").select_option    
  sleep 2         
end

Quando('submeto o meu cadastro sem nome') do
  find("#id_surname").set "Qualidade"
  find("#id_email").set Faker::Internet.free_email
  find("#id_password").set "123456"
  sleep 2
end

Então('devo ver a mensagem Preencha este campo. Meu cadastro nao deve ser efetuado e devo permanecer na pagina de cadastro.') do
  expect(page).to have_css ".col-12" 
end

Quando('submeto o meu cadastro sem email') do
  find("#id_first_name").set "Analista "
  find("#id_surname").set "Qualidade"
  find("#id_password").set "123456"
  sleep 2
end

Quando('submeto o meu cadastro sem sobrenome') do
  find("#id_first_name").set "Analista "
  find("#id_email").set Faker::Internet.free_email
  find("#id_password").set "123456"
  sleep 2
end

Quando('submeto o meu cadastro sem senha') do
  find("#id_first_name").set "Analista "
  find("#id_surname").set "Qualidade"
  find("#id_email").set Faker::Internet.free_email
  sleep 2
end

Quando('submeto o meu cadastro com os dados solicitados, porem não clico na checkbox para aceitar os termos de uso') do
  find("#id_first_name").set "Analista "
  find("#id_surname").set "Qualidade"
  find("#id_email").set Faker::Internet.free_email
  find("#id_password").set "123456"
  sleep 2
end

Então('devo ver a mensagem Marque esta caixa se deseja continuar. Meu cadastro nao deve ser efetuado e devo permanecer na pagina de cadastro.') do
  expect(page).to have_css ".helptext" 
end